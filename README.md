# repository_conflict_diseases

Repository for code on paper on conflicts and diseases

* Manuscript
	* https://peerj.com/preprints/27651/

	* http://indecs.eu/index.php?s=x&y=2019&p=598-614

* Basic SIR model	
	* SIR basic model.mmd

	* SIR basic model.txt

* SIR model with two armies

	* SIR_two_armies v2.mmd

	* SIR_two_armies v2.txt

* Model for end of conflict simulation

	* SIR_endofconflict.mmd

	* SIR_endofconflict.txt

* Integrated model with migration and breakdown in recovery from infections

	* SIR_integrated_model.mmd

	* SIR_integrated_model.txt

* Model with intervention

	* SIR_integrated_model_NOintervention.mmd

	* SIR_integrated_model_NOintervention.txt

* Model with no intervention

	* SIR_integrated_model_NOintervention.mmd

	* SIR_integrated_model_NOintervention.txt 

* Instructions for using Berkeley Madonna

	Download the Berkeley Madonna solver.

	www.berkeleymadonna.com/

      	Help on how to use Berkeley Madonna and helpful example commands are here

	http://www.berkeleymadonna.com/jmadonna/EquationHelp.htm

	After downloading it, you can run everything in "Demo Mode". After installing Berkeley Madonna, simply clicking on the .mmd files
will open up the graphical user interface. 

	Simply click on "Run" and go to the window (from Window in the menu bar) to see the output. The
sliders with parameters are already set up.


* Solve ODEs in a graphical user inteface in shiny R 

	* ODE_SOLVER_R_shiny.txt


* Contact

	* Soumya Banerjee

	* https://sites.google.com/site/neelsoumya/

* Other files


