


METHOD RK4

STARTTIME = 0
STOPTIME=600
DT = 0.0004

d/dt(S) = - beta*I*S
d/dt(I) = beta*I*S - v*I
d/dt(R) = v*I

; INITIAL STATE IN PRIMO INFECTION


;S_0 = 1e7
S_0 = 1e7
Init S = S_0
Init I = 1000
Init R = 0
beta = 1e-8
v = 0.020

Slog_perml = log10(S)

